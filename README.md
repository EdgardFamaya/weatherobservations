# README #


## What is this repository for? ##

# Weather Observations  #

Build a tool to mine the logs of a weather balloon for important information.

Requirements

There is a weather balloon traversing the globe, periodically taking observations. At each observation, the balloon records the temperature and its current location. When possible, the balloon relays this data back to observation posts on the ground.

for more detail about this requirement  please go to:

https://gist.github.com/dbousamra/66cab2034e4f212553fb


## Technical Implementation ##


Three programs have been created to tackle this requirement:

## MockObservation ## 
Create mocked weather observations by generating files with the required content of up to maxRowsPerFile lines,  the creation of those files  runs in parallel.

Finally it merges all the temp files into one big file and deletes the temp files.

To execute this program  please use as input parameters to the class:
	 
	         MockObservations totalNumOfRows maxRowsPerTempFile outputFilename
	         
	  Example of how to run it:
	  
	        MockObservations 5000000 1000000 MockedWeatherBallonData.dat
	    

## StatisticsBallonFlight ## 
This program will get next statistics from the weather data

*Min Temperature for all the flight
*Max Temperature for all the flight
*Mean Temperature for all the flight

  
  Temperatures will be given in Celsius
 
Given the challenge is to deal with big data I have implemented the solution with a Java 8 Stream which is a lazy object that use memory as needed and then it release it.

Additionally I'm using parallel Stream processing. The advantages of this parallel is seen when dealing with large files, but not with small processes, could be even slower.

 To execute this program  please use as input parameters to the class:
	 
	       StatisticsBallonFlight  inputFilename
	         
	  Example of how to run it:
	   
	     StatisticsBallonFlight MockedWeatherBallonData.dat


##  WeatherOutput ## 
This program will produce an output file of the weather observations with desired units for temperature and distance

 To execute this program  please use as input parameters to the class:
	 
	       WeatherOutput  uniTemperature unitDistance ouputFilename
	       
	       uniTemperature [Celsius | Kelvin | Farenheit]
	       unitDistance [kms | miles | meters]
	          
	   Example of how to run it:
	   
	     WeatherOutput Celsius kms WeatherBallonDataNormalized.dat




* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

## Summary of set up ##
## Configuration ##

*Java 8
*Ensure you have JUnit 4.0 in your path
*Clone this project
*Load it into your favorite Java IDE.


## Dependencies ##
None





* Deployment instructions
None

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
Edgard Amaya
famaya@yahoo.com
* Other community or team contact