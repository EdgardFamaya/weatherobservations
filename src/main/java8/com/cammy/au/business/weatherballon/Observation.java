package com.cammy.au.business.weatherballon;


/*
 * This class is the entity , all the data related to one observation
 * Additionally I add temperature in Celsius and distance in kms to have a unified unit of measurement
 * 
 */

import java.time.LocalDateTime;


public class Observation {
	private LocalDateTime date;
	private Location location;
	private int temperature;
	private String observatory;
	private boolean isValid;
	private int celsius; 
	private Location locationInKms=new Location();
	private int normalizedTemperature;
	private Location normalizedLocation=new Location();
	
	public int getCelsius() {
		return celsius;
	}


	public int getNormalizedTemperature() {
		return normalizedTemperature;
	}


	public void setNormalizedTemperature(String uniOfTemperature) {
		switch(uniOfTemperature.toUpperCase()) {
		
		case "KELVIN": this.normalizedTemperature=Conversion.celsiusToKelvin(celsius); break;
		case "CELSIUS": this.normalizedTemperature=celsius; break;
		case "FARENHEIT": this.normalizedTemperature=Conversion.celsiusToFahrenheit(celsius); break;
		default: this.normalizedTemperature=celsius; break;
		
	}
	}


	public void setCelsius(short celsius) {
		this.celsius = celsius;
	}


	public Observation(){}

	
	public boolean isValid() {
		return isValid;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	public LocalDateTime getDate() {
		return date;
	}
	public void setDate(LocalDateTime date) {
		this.date = date;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location, String observatory) {
		this.location = location;
		switch(observatory) {
			
			case "US": locationInKms.setX( Conversion.milesToKms(location.getX()));
					   locationInKms.setY( Conversion.milesToKms(location.getY()));
			           break;
			case "FR": locationInKms.setX( Conversion.mtsToKms(location.getX()));
					   locationInKms.setY( Conversion.mtsToKms(location.getY()));
			           break;
			default:
			case "AU": locationInKms = location; break;
			
		}
	}
	
	public void setNormilizedLocation(String unitOfDistance) {
		switch(unitOfDistance.toUpperCase()) {
			
			case "KMS": normalizedLocation = locationInKms;
			           break;
			case "MILES": locationInKms.setX( Conversion.kmsToMilesint(locationInKms.getX()));
					   locationInKms.setY( Conversion.kmsToMilesint(locationInKms.getY()));
			           break;
			case "METERS": locationInKms.setX( Conversion.kmsToMts(locationInKms.getX()));
						   locationInKms.setY( Conversion.kmsToMts(locationInKms.getY()));
				           break;
			default:normalizedLocation = locationInKms;
						break;
			
		}
	}
	
	
	public int getTemperature() {
		return temperature;
	}
	
	public void setTemperature(int temperature, String observatory) {
		this.temperature = temperature;
		switch(observatory) {
			case "AU": celsius = temperature; break;
			case "US": celsius = (int)Conversion.fahrenheitToCelsius(temperature); break;
			default: celsius = Conversion.kelvinToCelsius(temperature); break;
		}
	}
	
	public String getObservatory() {
		return observatory;
	}
	public void setObservatory(String observatory) {
		this.observatory = observatory;
	}
	

	@Override
	public String toString() {
		return "Observation [isValid=" + isValid +", date=" + date 
				+ ", location=" + location + ", locationInKms=" + locationInKms
				+ ", temperature=" + temperature  + ", celsius=" + celsius
				+ ", observatory=" + observatory + "]";
	}
	
	public String normilizedOutputToString() {
		return date + "|" 
				+ (int)normalizedLocation.getX() +"," 
				+ (int)normalizedLocation.getY() + "|" 
				+ normalizedTemperature + "|" 
				+ observatory ;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result
				+ ((location == null) ? 0 : location.hashCode());
		result = prime * result
				+ ((observatory == null) ? 0 : observatory.hashCode());
		result = prime * result + temperature;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Observation other = (Observation) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		if (observatory == null) {
			if (other.observatory != null)
				return false;
		} else if (!observatory.equals(other.observatory))
			return false;
		if (temperature != other.temperature)
			return false;
		return true;
	}
	
	

}
