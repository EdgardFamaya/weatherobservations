package com.cammy.au.business.weatherballon;

public class Conversion {
	
	/*
	 * This class will help to do all temperature and distance conversions
	 */
	
	//public Conversion(){}
	
	public static int celsiusToFahrenheit(int tempToConvert){
		return(  (int)((tempToConvert * (9D/5D) ) + 32));
	}
	
	public static int fahrenheitToCelsius(int tempToConvert){
		int farTemp= (int)((tempToConvert - 32 ) * (5D/9D)) ;
		return(  farTemp );
	}
	
	public static int celsiusToKelvin(int tempToConvert){
		return(  tempToConvert + 273 );
	}
	
	public  static int kelvinToCelsius(int tempToConvert){
		return(  tempToConvert - 273 );
	}
	
	public static int farenheitToKelvin(int tempToConvert){
		return( fahrenheitToCelsius(tempToConvert) + 273  );
	}
	
	public static int kelvinToFarenheit(int tempToConvert){
		return( ((9/5)*( tempToConvert -  + 273)) + 32  );
	}

	public static double kmsToMilesint (double distToConvert){
		return((distToConvert * 0.621372D) );
	}
	
	public static double milesToKms (double distToConvert){
		return(distToConvert / 0.621372D);
	}
	
	public static double kmsToMts(double distToConvert){
		//return(Math.round (distToConvert * 1000 )  );
		return( (distToConvert * 1000 )  );
	}
	
	public static double mtsToKms(double distToConvert){
		return((distToConvert / 1000D ) );
	}
}
