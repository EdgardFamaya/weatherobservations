package com.cammy.au.business.weatherballon;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveAction;

 
public class MockObservation extends RecursiveAction{
	
	/**
	 * Create mocked weather observations by generating files with the required content of up to maxRowsPerFile lines,
	 *  the creation of those files  runs in parallel.
	 * Finally it merges all the temp files into one big file and deletes the temp files.
	 * 
	 * To execute this program  please use as input parameters to the class:
	 * 
	 *         java MockObservations totalNumOfRows maxRowsPerTempFile outputFilename
	 *         
	 *  Example of how to run it:
	 *  
	 *    ./java MockObservations 5000000 1000000 MockedWeatherBallonData.dat
	 *    
	 */
	private static final long serialVersionUID = 1L;
	private long rowNumber;
	private static long maxRows;// = 5_000_000;
	private static long maxRowsPerFile;// = 1000_000;
	private long endRow;
	private long iniRow;
	private int fileNumber;
	private static String outputDataFileName;
	private static String howToRunMessage;
	
 
    public static void main(String[] args) throws FileNotFoundException {
    	
    	
    	//input parameters validation
    	howToRunMessage="Input has an invalid value. Please use:\n\n MockObservations totalNumOfRows maxRowsPerTempFile outputFilename"
	        		+ "\n\nExample:\n\n "
	        		+ "MockObservations 5000000 1000000 MockedWeatherBallonData.dat"
	        		+ "\n\n maxRowsPerTempFile should always be less or equal to totalNumOfRows"
	        		+ "\n PLease don't put a very big totalNumOfRows as you can quickly run out of disk space";
    	  
		if( args.length != 3 ||  args[0] == null ) {
	        System.out.println(  howToRunMessage);
	        System.exit(0);
	    }
		
		try {
			maxRows = Integer.parseInt(args[0]);
			maxRowsPerFile = Integer.parseInt(args[1]);
			outputDataFileName = (args[2]);
    		if (maxRows <= 0 || maxRowsPerFile<= 0 || maxRowsPerFile > maxRows || outputDataFileName == null ){
    			 System.out.println(  howToRunMessage);
    		        System.exit(0);
    		}

    	 } catch (NumberFormatException nfe) {	
    		 nfe.printStackTrace();
    	     
    	 }
 
    	System.out.println("\nFile creation STarted.\n");
    	
    	//Creating mocked data in parallel
    	
    	ForkJoinTask<?> task = new MockObservation(0,maxRows,0);
    	ForkJoinPool pool = new ForkJoinPool();
    	pool.invoke(task);
    	//End creation of mocked data
    	
    	System.out.println("\n");
    	System.out.println("All Individual temp files created.");
    	System.out.println("\n");
    	System.out.println("Merging Files.");
    	System.out.println("\n");
    	
    	//Merging .tmp files into one big file
    	Path path = Paths.get("./");
    	try{
    		
    		//Creating a list of MockedWeatherBallonData*.tmp files
    		String[] files = 
    				Files.list(path)
	    		.filter(p -> !Files.isDirectory(p))
	    		.filter( p  -> p.toString().contains("MockedWeatherBallonData") &&  p.toString().endsWith(".tmp")  )
	    		.map( p -> p.toAbsolutePath())
	    		.map(s->s.toString())
	    		.toArray(size -> new String[size]);

    		//Reading one by one the *.tmp files and appending them  to one big MockedWeatherBallonData.dat file
    		Path pathMerge = Paths.get(outputDataFileName);
    		try ( PrintWriter pw = new PrintWriter( Files.newBufferedWriter(pathMerge) )){
    			for (String fileToMerge: files){
    				
    				System.out.println("Merging File candidate: " + fileToMerge);
    				
    				Path pathFileToMerge = Paths.get(fileToMerge);
    				try{Files
							.lines(pathFileToMerge, StandardCharsets.UTF_8)
							.map(s ->  s.toString())
							.forEach(pw::println);
    						
    					
    				}catch(IOException e){
    					e.printStackTrace();;
    				}
    				
    				
    			}
    			
    		} catch (IOException e) {
    			System.out.println("hola hol");
            	e.printStackTrace();
            }
    		
    		
    		//deletimg .tmp files
        	
        	System.out.println("\n");
        	System.out.println("Deleting *tmp files .\n");
        	
        	for (String fileToMerge: files){
        		System.out.println("Deleting File : " + fileToMerge);
				
				Path pathFileToDelete = Paths.get(fileToMerge);
				Files.deleteIfExists(pathFileToDelete);
        		
        	}
        	
    		
    	} catch (IOException ioe1){
    		ioe1.printStackTrace();
    	}
    	
    	
    
    	System.out.println("\nFile creation completed.\n");
    	
    }
    
    public MockObservation(long iniRow, long endRow, int fileNumber){
    	this.iniRow = iniRow;
    	this.endRow = endRow;
    	this.fileNumber = fileNumber;
    	//System.out.println("Constructor = fileNumber: "+fileNumber + ", iniRow=" + iniRow + ", endRow=" + endRow );
    	
    }
    
    
    protected void compute(){
    	/*
    	 * This function will recusrsivelly divid up to maxRowsPerFile and at that point it will
    	 * generate random data
    	 */
    	
    	if(endRow - iniRow <= maxRowsPerFile){
    		int x,y;
    		int randomTemp;
    		int randomObservatory;
    		String observatory;
    		System.out.println("\nCreating file= MockedWeatherBallonData" + fileNumber + ".tmp with iniRow = " + iniRow + ", endRow=" + endRow);
    		String fileName="MockedWeatherBallonData" + fileNumber + ".tmp";
    		
    		Path path = Paths.get(fileName);

    		try ( BufferedWriter writer = Files.newBufferedWriter(path, StandardOpenOption.CREATE)){
    			for(long i= iniRow; i< endRow; i++ ){
    				//random location
    				x= (int)(Math.random() * ((100 - 1) + 1)) + 1;
    				y= (int)(Math.random() * ((100 - 1) + 1)) + 1;
    				//System.out.println("x: "+x+", y:" + y);
    				//random date
    				Random random = new Random();
    				int minSecond = (int) LocalDateTime.of(2017, 1, 1,0,0).toEpochSecond(ZoneOffset.UTC);
    				int maxSecond = (int) LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);
    				long randomDay = minSecond + random.nextInt(maxSecond - minSecond);
    			    LocalDateTime randomDate = LocalDateTime.ofEpochSecond(randomDay, 0, ZoneOffset.UTC);
    				//LocalDateTime randomDate = LocalDateTime.of(2017, 8, 01, 00, 00);//.ofEpochSecond(randomDay, 0, ZoneOffset.UTC);
    				//System.out.println("randomDate: "+ randomDate);
    				
    				
    				//random temperature
    				randomTemp= (int)(Math.random() * ((250 - 1) + 1)) + 1;
    				//System.out.println("randomTemp: "+ randomTemp);
    				
    				//random observatory
    				randomObservatory= (int)(Math.random() * ((7 - 1) + 1)) + 1;
    				switch(randomObservatory){
    					case 1 : observatory="AU"; break;
    					case 2 : observatory="US"; break;
    					case 3 : observatory="FR"; break;
    					case 4 : observatory="CO"; break;
    					case 5 : observatory="GB"; break;
    					case 6 : observatory="RS"; break;
    					case 7 : observatory="CH"; break;
    					default : observatory="XX"; break;
    				}
    				
    				//System.out.println("observatory: "+ observatory);
    				
    				writer.write(randomDate + "|" + x+","+y+ "|" + randomTemp + "|" + observatory + "\n");
    				
    			}
    		} catch (IOException e) {
    			System.out.println("hola hol");
            	e.printStackTrace();
            }
    	}else {
    		rowNumber = (int)(iniRow+((endRow - iniRow)/2D));
    		System.out.println("iniRow= " + iniRow + ", rowNumber= " + rowNumber + ", endRow= " + endRow);
    		invokeAll( new MockObservation(iniRow, rowNumber, (fileNumber + 1) * 7),
    				   new MockObservation(rowNumber, endRow, (fileNumber + 2) * 7)
    				);
    	}
    }
 
}
 