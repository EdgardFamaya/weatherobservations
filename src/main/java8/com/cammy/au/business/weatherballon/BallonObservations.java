package com.cammy.au.business.weatherballon;


import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.stream.Stream;

/*
 * Class with components to read the data file in a Stream
 * To be extended as needed for statistics, or produce the normalized output data
 * 
 */

public class BallonObservations {
	protected static Stream<Observation> weatherSamples;
	protected static Stream<Observation> weatherSamples2;
	protected static String inputDataFileName;
	
	
	protected static  boolean readWeatherDataFile(){
		
		System.out.println("Reading datafile into Stream at: " + LocalTime.now() + "\n");	
		
		Path path = Paths.get(inputDataFileName);
		try{/*
			//to delete, I'm leaving this here just in case I have to re test
			Files
			.lines(path, StandardCharsets.UTF_8)
			.parallel()
			.map(s -> convertRecordToObservation(s) )
			.forEach(System.out::println);
			*/
			
			weatherSamples =
			 Files
				.lines(path, StandardCharsets.UTF_8)
				//.parallel()
				.map(s -> convertRecordToObservation(s) );
			
			System.out.println("Reading datafile completed at:"  + LocalTime.now() + "\n");	

		}catch(IOException e){
			handleException(e);
		}
		return (true);
		
	}
	
	

	protected static Observation convertRecordToObservation( String line){
		
		Observation observation = new Observation();
		
		DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;//.ofPattern("yyyy-MM-dd hh:mm");
		
		String[] tokens = line.split("\\|");
		 try{
			 observation.setDate(LocalDateTime.parse(tokens[0],formatter));
			 observation.setLocation(isValidLocation(tokens[1]),  tokens[3]);
			 observation.setTemperature( Integer.parseInt(tokens[2]), tokens[3]);
			 observation.setObservatory(tokens[3]);
			 observation.setValid(true);
			 
		 }catch(ArrayIndexOutOfBoundsException | DateTimeParseException | NumberFormatException |LocationException e){
			 observation.setValid(false);
		 }
		 
		 return (observation);
	}
	
	
	
	protected static Location isValidLocation(String strLocation) throws LocationException,
	                      NumberFormatException, ArrayIndexOutOfBoundsException {
		Location location = new Location();
		String tokens[] = strLocation.split("\\,");
		
		location.setX(Integer.parseInt(tokens[0]));
		location.setY(Integer.parseInt(tokens[1]));
		
		return location;		
	}
	
	
	

	private static void handleException(Exception e){
		e.printStackTrace();
		throw new RuntimeException();
		
	}
}

class LocationException extends Throwable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public LocationException(){
		
	}
	public  LocationException(Exception e){
		super( e);
	}
}
