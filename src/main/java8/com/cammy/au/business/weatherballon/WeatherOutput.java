package com.cammy.au.business.weatherballon;



import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

/*
 * This program will produce an output file of the weather observations with desired units for temperature and distance
 * 
 * Given the challenge is to deal with big data I have implemented the solution with a Java 8 Stream
 * which is a lazy object that use memory as needed and then it release it.
 * 
 * Additionally I'm using parallel Stream processing. The advantages of this parallel is seen when 
 * dealing with large files, but not with small processes, could be even slower.
 * 
 * To execute this program  please use as input parameters to the class:
	 * 
	 *       WeatherOutput  uniTemperature unitDistance ouputFilename
	 *       
	 *       uniTemperature [Celsius | Kelvin | Farenheit]
	 *       unitDistance [kms | miles | meters]
	 *         
	 *  Example of how to run it:
	 *  
	 *    WeatherOutput Celsius kms WeatherBallonDataNormalized.dat
 * 
 */

public class WeatherOutput extends BallonObservations{
	private static String uniOfTemperature;
	private static String unitOfDistance;
	private static String howToRunMessage;
	private static String outputDataFileName;

	
	public  static void main (String[] args){
		
		long startTime = System.nanoTime();
	
		
		howToRunMessage="Input is invalid. Please use:\n\n WeatherOutput  uniOfTemperature unitOfDistance inputDataFile ouputFilename"
				    + "\n\nuniTemperature [Celsius | Kelvin | Farenheit]"
				    + "\nunitDistance [kms | miles | meters]"
	        		+ "\n\nExample:\n\n "
	        		+ "WeatherOutput Celsius kms MockedWeatherBallonData.dat WeatherBallonDataNormalized.dat"
	        		+ "\n";
		  
		if( args.length != 4 ||  args[0] == null ) {
	        System.out.println(  howToRunMessage);
	        System.exit(0);
	    }
		
		try {
			uniOfTemperature = (args[0]);
			unitOfDistance = (args[1]);
			inputDataFileName = (args[2]);
			outputDataFileName = (args[3]);
    		if (outputDataFileName == null || inputDataFileName == null 
    				|| ( !uniOfTemperature.equalsIgnoreCase("Celsius") 
    				   && !uniOfTemperature.equalsIgnoreCase("Kelvin")  
    				   && !uniOfTemperature.equalsIgnoreCase("Farenheit") )
    				|| ( !unitOfDistance.equalsIgnoreCase("kms")
    				   && !unitOfDistance.equalsIgnoreCase("miles")
    				   && !unitOfDistance.equalsIgnoreCase("meters") 
    				)){
    			 System.out.println(  howToRunMessage);
    		        System.exit(0);
    		}

    	 } catch (NumberFormatException nfe) {	
    		 nfe.printStackTrace();
    	     
    	 }
		
		
			
		//reading the file and populating a Stream
		@SuppressWarnings("unused")
		boolean readFlagOk = readWeatherDataFile();
		
	    //genarting normilized output
		@SuppressWarnings("unused")
		boolean statsOk =generateOutputFile();
		
		long endTime = System.nanoTime();
        long elapsedTimeInMillis = TimeUnit.MILLISECONDS.convert((endTime - startTime), TimeUnit.NANOSECONDS);
        

	     System.out.println("\nNormilized Output File created"
	    		 + "\nTime Spent= " + elapsedTimeInMillis + " ms");
	}
	
	

	private static boolean generateOutputFile(){
	
		Path pathMerge = Paths.get(outputDataFileName);
		try ( PrintWriter pw = new PrintWriter( Files.newBufferedWriter(pathMerge) )){
				weatherSamples
				.filter(s -> s.isValid())
				.map( s -> normalizeObservation(s))
				.map(s -> s.normilizedOutputToString())
				.forEach(pw::println);
				
		} catch (IOException e) {
			System.out.println("hola hol");
        	e.printStackTrace();
        }
		
		return (true);
	}
	
	private static Observation normalizeObservation(Observation observToChange){
		Observation newObservation = new Observation();
		
		newObservation = observToChange;
		
		newObservation.setNormalizedTemperature(uniOfTemperature);
		newObservation.setNormilizedLocation(unitOfDistance);
		
		return newObservation;
	}

	

	
}
