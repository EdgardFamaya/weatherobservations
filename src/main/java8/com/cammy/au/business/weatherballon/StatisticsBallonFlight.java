package com.cammy.au.business.weatherballon;

import java.util.IntSummaryStatistics;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/*
 * This program will get next statistics from the weather data
 * 
 * Min Temperature for all the flight
 * Max Temperature for all the flight
 * Mean Temperature for all the flight
 * 
 * Temperatures will be given in Celsius
 * 
 * Given the challenge is to deal with big data I have implemented the solution with a Java 8 Stream
 * which is a lazy object that use memory as needed and then it release it.
 * 
 * Additionally I'm using parallel Stream processing. The advantages of this parallel is seen when 
 * dealing with large files, but not with small processes, could be even slower.
 * 
 * To execute this program  please use as input parameters to the class:
	 * 
	 *       StatisticsBallonFlight  inputFilename
	 *         
	 *  Example of how to run it:
	 *  
	 *    StatisticsBallonFlight MockedWeatherBallonData.dat
 * 
 */

public class StatisticsBallonFlight extends BallonObservations{
	private static int minTemperature;
	private static int maxTemperature;
	private static double  meanTemperature;
	private static long numOfRowsProcessed;
	private static String howToRunMessage;

	
	public  static void main (String[] args){
		
		long startTime = System.nanoTime();
	
		
		howToRunMessage="Input is invalid. Please use:\n\n StatisticsBallonFlight inputFilename"
	        		+ "\n\nExample:\n\n "
	        		+ "StatisticsBallonFlight MockedWeatherBallonData.dat"
	        		+ "\n";
		  
		if( args.length != 1 ||  args[0] == null ) {
	        System.out.println(  howToRunMessage);
	        System.exit(0);
	    }
		
		inputDataFileName = (args[0]);
		
			
		//reading the file and populating a Stream
		@SuppressWarnings("unused")
		boolean readFlagOk = readWeatherDataFile();
			
			
		@SuppressWarnings("unused")
		boolean statsOk =getStatsFromTemperature();
		//	int minTemp = getMinTemperature();
		
		long endTime = System.nanoTime();
        long elapsedTimeInMillis = TimeUnit.MILLISECONDS.convert((endTime - startTime), TimeUnit.NANOSECONDS);
        
		System.out.println("Temperatures in Celsius\n");	
	     System.out.println("Minimum Temperarure= " + minTemperature + "\nMaximum Temperature= " + maxTemperature 
	    		 + "\nMean Temperature= " + meanTemperature
	    		 + "\nNumber Of Rows Processed= " + numOfRowsProcessed
	    		 + "\nTime Spent= " + elapsedTimeInMillis + " ms");
	}
	
	

	@SuppressWarnings("unused")
	private static int getMinTemperatureAux(){
		int  minTemperature=0;
		Observation observation=new Observation();
		Optional<Observation> minObservation = weatherSamples2
				.filter(s -> s.isValid())
				.min( (obs1,obs2) -> obs1.getCelsius() - obs2.getCelsius() );
		if (minObservation.isPresent()) {
			observation=minObservation.get();
			minTemperature=observation.getCelsius();
			
		}
		return (minTemperature);
	}
	
	
	private static boolean getStatsFromTemperature(){
		
		IntSummaryStatistics stats = weatherSamples
				.filter(s -> s.isValid())
				.collect(Collectors.summarizingInt(Observation::getCelsius));
		
		maxTemperature=stats.getMax();
		minTemperature = stats.getMin();
		meanTemperature= stats.getAverage();
		numOfRowsProcessed=stats.getCount();
		
		return (true);
	}
	
	
	
	public static int getMinTemperature() {
		return minTemperature;
	}
	
	public static int getMaxTemperature() {
		return maxTemperature;
	}

	public static double getMeanTemperature() {
		return meanTemperature;
	}

	

	
}
/*
class LocationException extends Throwable{
	
	private static final long serialVersionUID = 1L;
	
	public LocationException(){
		
	}
	public  LocationException(Exception e){
		super( e);
	}
}
*/
